package controller;

import api.IDivvyTripsManager;
import model.data_structures.IQueue;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations(String file) {
		//DONE Pasarle el archivo a cargar en la p�g principal o aqu�
		manager.loadStations(file);

	}
	
	public static void loadTrips() {
		manager.loadTrips();
	}
		
	public static IQueue <String> getLastNStations (int bicycleId, int n) {
		return manager.getLastNStations(bicycleId, n);
	}
	
	public static VOTrip customerNumberN (int stationID, int n) {
		return manager.customerNumberN(stationID, n);
	}
}
