package model.vo;

/**
 * Representation of a byke object
 */
public class VOBike {

private int id; 
	
	private String nombre;
	
	private String ciudad;
	
	private double latitud;
	
	private double longitud;
	
	private int capacidad;
	
	private String online_date;
	
	public VOBike (int pId, String pNombre, String pCiudad, double pLatitud, double pLongitud, int pCapacidad, String pDate)
	{
		id = pId;
		nombre = pNombre;
		ciudad = pCiudad;
		latitud = pLatitud;
		longitud = pLongitud;
		capacidad = pCapacidad;
		online_date = pDate;
	}
	
	public int darId()
	{
		return id;
	}
	
	public String darNombre ()
	{
		return nombre;
	}
	
	public String darCiudad()
	{
		return ciudad;
	}
	
	public double darLatitud()
	{
		return latitud;
	}
	
	public double darLongitud()
	{
		return longitud;
	}
	
	public int darCapacidad()
	{
		return capacidad;
	}
	
	public String darFecha()
	{
		return online_date;
	}
}
