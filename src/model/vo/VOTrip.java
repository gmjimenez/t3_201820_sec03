package model.vo;

/**
 * Representation of a Trip object
 * @param <T>
 */
public class VOTrip<T> implements Comparable<T> {

	private String tripId; 
	
	private String gender; 
	
	private String usertype; 
	
	private String bikeId; 
	
	private String birthyear;
	
	private String fromStationName;
	
	private String toStationName;
	
	private String fromStationId;
	
	private String toStationId;
	
	private String tripDuration;
	
	private String startTime;
	
	private String endTime; 
	
	//
	
	private int idStation;
	private String name;
	private String city;
	private double latitude;
	private double longitude;
	private int capacity;
	private String onlineDate;
	
	
	

	public VOTrip(String pId, String pStartTime, String pEndTime, String pBikeId, String pDuration, String pFromSId, String pFromSName, String pToSId, String toSName, String pUsertype, String pGender, String birth) {
		tripDuration = pDuration;
		tripId = pId;
		fromStationName = pFromSName;
		toStationName = toSName; 
		gender = pGender;
		startTime = pStartTime;
		toStationId = pToSId;
		fromStationId = pFromSId;
		endTime = pEndTime; 
		birthyear = birth; 
		bikeId = pBikeId;
		usertype = pUsertype;
	}
	
	public VOTrip(int id, String pName, String pCity, double pLatitude, double pLongitude, int pCapacity, String date) {
		idStation = id; 
		name = pName; 
		city = pCity;
		latitude = pLatitude; 
		longitude = pLongitude; 
		capacity = pCapacity;
		onlineDate = date; 
		
	}
	/**
	 * @return id - Trip_id
	 */
	public String id() {
		// DONE Auto-generated method stub
		return tripId;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public String getTripSeconds() {
		// DONE Auto-generated method stub
		return tripDuration;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// DONE Auto-generated method stub
		return fromStationName;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// DONE Auto-generated method stub
		return toStationName;
	}
	
	public String getGender() {
		return gender; 
	}
	
	public int getStationId() {
		return idStation; 
	}
	
	public String getToStationId() {
		return toStationId;
	}
	@Override
	public int compareTo(T arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
}
