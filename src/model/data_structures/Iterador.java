package model.data_structures;

import java.io.Serializable;

import java.util.ListIterator;

/**
 * Clase que representa el iterador de la lista 
 * @param <E> objeto que almacena el iterador 
 */
public class Iterador<E> implements ListIterator<E>, Serializable 
{

	//-------------------------------------------
	//CONSTANTES
	//--------------------------------------------
	
	/**
	 * Constante de serializacion
	 */
	private static final long serialVersionUID = 1L;
	
	
	//---------------------------------------------
	//ATRIBUTOS
	//---------------------------------------------
	
	private Nodo<E> anterior;

	private Nodo<E> actual;

	
	//----------------------------------------------
	//METODOS
	//----------------------------------------------
	
	/**
	 * 
	 * @param primerNodo
	 */
	public Iterador(Nodo<E> primerNodo)
	{
		actual = primerNodo;
		System.out.println(actual!=null? "no es null": "si es null");
		System.out.println(primerNodo!=null? "no es null": "si es null el primer nodo");
		anterior = null;
	}
	
    /**
     * 
     */
	public boolean hasNext() 
	{
		boolean respuesta = false;
		
		if(actual.darSgte()!=null)
		{
			respuesta = true;
		}
		return respuesta;
	}

    /**
     * 
     */
	public boolean hasPrevious() 
	{
		boolean respuesta = false;
		
		if(anterior!=null)
		{
			respuesta = true;
		}
		return respuesta;
	}

    /**
     * 
     */
	public E next() 
	{
		anterior = actual;
		actual = (Nodo<E>) actual.darSgte();
		return actual.darItem();
	}

    /**
     * 
     */
	public E previous() 
	{
		actual = anterior;
		anterior = anterior.darAnt();
		return actual.darItem();
	}

	@Override
	public void add(E arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int nextIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int previousIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void set(E arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
}