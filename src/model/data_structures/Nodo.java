package model.data_structures;



public class Nodo<T> 
{
	//--------------------------------
	//Atributos
	//--------------------------------
	
	private Nodo<T> siguiente; 

	private Nodo<T> anterior; 

	private T item;



	//----------------------------------------------
	//METODOS
	//----------------------------------------------
	
	public Nodo(T t) 
	{
		item = t; 
	}

	public T darItem() 
	{
		return item; 
	}

	public Nodo<T> darSgte() 
	{
		return siguiente;

	}

	public void cambiarSiguiente( Nodo<T> nSiguiente )
	{
		this.siguiente = nSiguiente;
	}

	public void desconectarSiguiente( )
	{
		siguiente = siguiente.siguiente;
	}


	public void insertarDespues( Nodo<T> pNode )
	{
		pNode.siguiente = siguiente;
		pNode.anterior =this;

		if(siguiente !=null) 
		{
			siguiente.anterior = pNode;
		}
		siguiente = pNode;
	}




	public Nodo<T> darAnt() 
	{
		return anterior;

	}

	public void cambiarAnt( Nodo<T> ante )
	{
		this.anterior = ante;

	}

	public void desconectarAnterior( )
	{
		anterior = anterior.anterior;
	}

	public void insertarAntes( Nodo<T> pNode )
	{
		pNode.siguiente = this;
		pNode.anterior = anterior;
		
		if(anterior !=null)
			anterior.siguiente = pNode;
		
		anterior = pNode;
	}

	public void eliminarActual() {
		if(anterior!=null)
			anterior.siguiente = siguiente;

		if(siguiente!=null)
			siguiente.anterior = anterior;
	}


}

