package model.data_structures;

import java.util.Iterator;

public class Queue<E> implements IQueue<E>
{

	//----------------------------
	//ATRIBUTOS
	//----------------------------

	protected Nodo<E> primerNodo;

	protected int size;

	protected Nodo<E> ultimo;


	//------------------------------
	//METODOS
	//------------------------------

	/**
	 * 	
	 */
	public Queue()
	{
		primerNodo = null;
		size = 0;
		ultimo = null;
	}

	/**
	 * 
	 * @param primero
	 */
	public Queue(E primero)
	{
		primerNodo = new Nodo<E>(primero);
		size = 1;
		ultimo = primerNodo;
	}


	/**
	 * 
	 * @return
	 */
	public Nodo<E> getPrimerNodo()
	{
		return primerNodo;
	}

	/**
	 * 
	 * @return
	 */
	public Nodo<E> ultimoNodo()
	{
		return ultimo;
	}


	/**
	 * 
	 */
	@Override
	public Iterator<E> iterator() 
	{
		Iterador<E> iterador = new Iterador<E>(primerNodo);
		return iterador;
	}

	/**
	 * 
	 */
	@Override
	public boolean isEmpty() 
	{
		Boolean isEmpty = true;

		if(primerNodo!=null)
		{
			isEmpty = false;
		}
		return isEmpty;
	}

	/**
	 * 
	 */
	@Override
	public int size() 
	{
		return size;
	}


	/**
	 * 
	 */
	@Override
	public void enqueue(Object t) 
	{
		Nodo<E> nuevo = new Nodo<E>((E) t);

		if(primerNodo!=null)
		{
			ultimo.cambiarSiguiente(nuevo);
			nuevo.cambiarAnt(ultimo);
			ultimo = nuevo;
			size++;
		}
		else
		{
			primerNodo = nuevo;
			ultimo = nuevo;
			size++;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public E dequeue() 
	{
		Object sacado = null;

		if(primerNodo!=null)
		{
			sacado = primerNodo;
			Nodo<E> siguiente = primerNodo.darSgte();

			if(siguiente!=null)
			{
				primerNodo = siguiente;
				siguiente.cambiarAnt(null);
				size--;
			}
			else
			{
				primerNodo = null;
				size--;
			}
		}
		return (E) sacado;
	}

}