package model.data_structures;

import java.util.Iterator;

public class Stack<T extends Comparable<T>> implements IStack<T> {

	private int size; 
	private Nodo<T> primerNodo; 
	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEmpty() {
		// DONE Auto-generated method stub
		if(size()==0)
			return true;
		return false; 
	}

	@Override
	public int size() {
		// DONE Auto-generated method stub
		size = 0; 
		Nodo<T> actual = primerNodo; 
		while(actual !=null) {
			size++;
			actual = actual.darSgte();
		}
		
		return size;
		
	}

	@Override
	public void push(T t) {
		// TODO Auto-generated method stub
		//Insertar un elemento en el "tope" de la  lista
		//Insertar al inicio para poder hacer pop() al inicio. 

		if(primerNodo == null) {
			primerNodo = new Nodo<>(t);
			size++; 
		}
		else {
			//Hacer que se a�ada al inicio
			Nodo<T> oldFirst = primerNodo;
			primerNodo = new Nodo<>(t);
			primerNodo.cambiarSiguiente(oldFirst);
			size++;
		}


	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		//Saca un �ltimo elemento insertado
		if(primerNodo==null)
			return null;
		else{
			T item = (T) primerNodo.darItem();
			primerNodo = primerNodo.darSgte();
			return item; 
		}

	}

	

	

}