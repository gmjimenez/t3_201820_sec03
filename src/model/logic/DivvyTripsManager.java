package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import api.IDivvyTripsManager;
import model.vo.VOBike;
import model.vo.VOTrip;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager implements IDivvyTripsManager {

	private IStack pilaViajes; 
	private Queue<VOBike> colaViajes; 
	private IStack pilaEstacion;
	
	public void loadStations (String stationsFile) {
		// DONE Auto-generated method stub
		File arch = new File(stationsFile);
		pilaEstacion = new Stack<>();

		try {
			FileReader reader = new FileReader(arch);
			BufferedReader in = new BufferedReader(reader);
			String linea = in.readLine();
			linea = in.readLine();

			while(linea!=null) {

				String[] viajesTotal = linea.split(",");
				
				
				int id = Integer.parseInt(viajesTotal[0]);
				String name = viajesTotal[1];
				String city = viajesTotal[2];
				double latitude = Double.parseDouble(viajesTotal[3]);
				double longitude = Double.parseDouble(viajesTotal[4]);
				int capacity =Integer.parseInt(viajesTotal[5]);
				String onlineDate = viajesTotal[6];
				
				VOTrip stationP = new VOTrip(id, name, city, latitude, longitude, capacity, onlineDate);
				pilaEstacion.push(stationP);

				
				linea = in.readLine();
			}

			in.close();
			reader.close();


		} catch ( IOException k) {
			// DONE Auto-generated catch block


		}
	}

		

	
	public void loadTrips () {
		// DONE Auto-generated method stub
		File arch = new File("./data/Divvy_Trips_2017_Q3.csv");

		try {
			FileReader reader = new FileReader(arch);
			BufferedReader in = new BufferedReader(reader);
			String linea = in.readLine();
			linea = in.readLine();
			pilaViajes = new Stack<>();
			colaViajes = new Queue<>();


			while(linea!=null) {

				String[] viajes = linea.split(",");
				
					String pId = viajes[0];
					String pStartTime = viajes[1];
					String pEndTime = viajes[2];
					String pBikeId = viajes[3];
					String pDuration = viajes[4];
					String pFromSId = viajes[5];
					String pFromSName = viajes[6];
					String pToSId = viajes[7];
					String toSName = viajes[8];
					String pUsertype = viajes[9];
					String pGender = viajes[10];
					String pBirth = viajes[11];
					VOTrip trip= new VOTrip(pId, pStartTime, pEndTime, pBikeId, pDuration, pFromSId, pFromSName, pToSId, toSName, pUsertype, pGender, pBirth);
					//A�adir a la lista
					pilaViajes.push(trip); //Problema
					System.out.println(linea);
					colaViajes.enqueue(trip); //Este funciona cuando se implemente Queue
					
					
				

				linea = in.readLine();
			}
			in.close();
			reader.close();


		} catch ( IOException k) {
			// DONE Auto-generated catch block


		}
		
	}
	
	
	/**
	 * 
	 */
	@Override
	public IQueue <String> getLastNStations (int bicycleId, int n) 
	{
		//metodo Daniel
		IQueue estaciones = null;
		
		if(colaViajes.isEmpty())
		{
			estaciones = null;
		}
		else
		{
			int contador = 0;
			
			while(colaViajes.iterator().hasNext() && contador < n)
			{
				if(colaViajes.getPrimerNodo().darItem().darId() == bicycleId)
				{
					estaciones.enqueue(colaViajes.getPrimerNodo().darItem().darNombre());
					contador ++;
				}
				else 
				{
					colaViajes.iterator().next();
				}
			}
		}
		
		return estaciones;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {
		// DONE Auto-generated method stub
		int count=0; 
		VOTrip buscado = null;
		//Iterar sobre el stack e ir cambiando la referencia del viaje hasta que llegue a n
		for (int i = 0; i < pilaViajes.size(); i++) {
			VOTrip viaje = (VOTrip) pilaViajes.pop();
			if(viaje.getToStationId().equals(Integer.toString(stationID))) {
				
				count++;
				buscado = viaje; 
				;
				
				if(count == n) {
					return buscado; 
				}
			}
		}
		return buscado; 
	}	


}
