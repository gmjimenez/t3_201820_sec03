

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.data_structures.IStack;
import model.data_structures.Stack;
import model.vo.VOTrip;

class StackTest {
	@Test
	void testEmpty() {
		
		try {
			IStack n =  new Stack();
			VOTrip i = new VOTrip("16734065","9/30/2017 23:59:58","10/1/2017 00:05:47","1411","349","216","California Ave & Division St","259","California Ave & Francis Pl","Subscriber","Male","1985");
			n.push(i);
			assertFalse(n.isEmpty());
		} catch (Exception e) {
			// TODO: handle exception
			fail("It must not be empty");
		}
		
	}

	@Test
	void testSize() {
		try {
			IStack n =  new Stack();
			VOTrip i = new VOTrip("16734065","9/30/2017 23:59:58","10/1/2017 00:05:47","1411","349","216","California Ave & Division St","259","California Ave & Francis Pl","Subscriber","Male","1985");
			n.push(i);
			int size =n.size();
			assertTrue(size == 1);
		}
		catch(Exception e) {
			fail("El tamaño debe ser 1");
		}
	}


	@Test
	void testPush() {
		try {
			IStack n =  new Stack();
			VOTrip i = new VOTrip("16734065","9/30/2017 23:59:58","10/1/2017 00:05:47","1411","349","216","California Ave & Division St","259","California Ave & Francis Pl","Subscriber","Male","1985");
			VOTrip first = new VOTrip("16734064","9/30/2017 23:59:53","10/1/2017 00:05:47","3048","354","216","California Ave & Division St","259","California Ave & Francis Pl","Subscriber","Male","1979");
			n.push(i);
			n.push(first);
			VOTrip extraido = n.pop();
			assertEquals(extraido, first);
			
		} catch (Exception e) {
			// TODO: handle exception
			fail("Deben ser iguales");
		}

	}


	@Test
	void testPop() {
		
		try {
			IStack n =  new Stack();
			VOTrip extraido = n.pop();
			assertNull(extraido);
		} catch (Exception e) {
			// TODO: handle exception
			fail("It must be null");
		}

	}

}
